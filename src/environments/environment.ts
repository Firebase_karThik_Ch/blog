// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig: {
    apiKey: "AIzaSyDy2VW29s9mB2kiRs7g44K8OitNt6J_0A8",
    authDomain: "angular-auth-74081.firebaseapp.com",
    databaseURL: "https://angular-auth-74081-default-rtdb.firebaseio.com",
    projectId: "angular-auth-74081",
    storageBucket: "angular-auth-74081.appspot.com",
    messagingSenderId: "384540958953",
    appId: "1:384540958953:web:8dc08cbf7899680a209078",
    measurementId: "G-5QKDB0PL5N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
