import { Injectable ,NgZone} from '@angular/core';
// import { User } from "../shared/services/user";
import  auth  from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from "@angular/router";
import firebase from 'firebase/app';
import { ToastServiceService } from './toast-service.service';


export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
}

@Injectable({
  providedIn: 'root'
})


export class ApiService {
  userData: any; // Save logged in user data

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,  
    public ngZone: NgZone, // NgZone service to remove outside scope warning
    private  toast:ToastServiceService
  ) { 

    this.afAuth.authState.subscribe(user=>{
        if(user){
          this.userData =user;
          localStorage.setItem('user',JSON.stringify(this.userData));
          JSON.parse(localStorage.getItem('user')!);

        }
        else{
          localStorage.setItem('user', null!);
          JSON.parse(localStorage.getItem('user')!);

        }
    })
  }


  // Sign in with email/password
  SignIn(data :any) {
    return this.afAuth.signInWithEmailAndPassword(data.email, data.password)
      .then((result :any) => {
        this.ngZone.run(() => {
          // console.log()
          if(!result.user.emailVerified){
            this.SendVerificationMail();
            this.toast.show('Please verify your mail',{classname: 'bg-success text-light'});
            

          }
          else{
            this.toast.show('Signnin Successfull',{classname: 'bg-success text-light'})
            this.router.navigate(['blog/list']);
          }
          
        });
        this.SetUserData(result.user);
      }).catch((error :any) => {
        // window.alert(error.message)
        this.toast.show(error.message,{classname: 'bg-danger text-light'})

      })
  }

  // Sign up with email/password
  SignUp(data:any) {
    return this.afAuth.createUserWithEmailAndPassword(data.email, data.password)
      .then((result) => {
        // this.toast.show('Please Check your mail',{classname: 'bg-success text-light'})
        console.log(result)
        this.SendVerificationMail();
        this.SetUserData(result.user);
      }).catch((error) => {
        // window.alert(error.message)
        this.toast.show(error.message,{classname: 'bg-danger text-light'})

      })
  }

   
  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.currentUser.then(u => u?.sendEmailVerification())
    .then(() => {
      // this.router.navigate(['email-verification']);
      this.toast.show('Please verify your mail',{classname: 'bg-success text-light'})

    })
} 
ForgotPassword(passwordResetEmail:string) {
  return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
  .then(() => {
    window.alert('Password reset email sent, check your inbox.');
  }).catch((error) => {
    window.alert(error)
  })
}

get isLoggedIn(): boolean {
  const user = JSON.parse(localStorage.getItem('user')!);
  return (user !== null && user.emailVerified !== false) ? true : false;
}

GoogleAuth() {
  return this.AuthLogin(new firebase.auth.GoogleAuthProvider());


}
FacebookAuth() {
  return this.AuthLogin(new firebase.auth.FacebookAuthProvider());


}
LinkAuth() {
  // return this.AuthLogin(new firebase.auth.MultiFactorAssertion());
  // return this.afAuth.l(new firebase.auth.FacebookAuthProvider());

}
AuthLogin(provider:any) {
  return this.afAuth.signInWithPopup(provider)
  .then((result) => 
  
  {
     this.ngZone.run(() => {
        this.router.navigate(['/blog/list']);
      })
    this.SetUserData(result.user);
  }).catch((error) => {
    window.alert(error)
  })
}



   /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  SetUserData(user :any) {
    
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  SignOut(){
    return this.afAuth.signOut().then(() =>{
      localStorage.removeItem('user');
      this.router.navigate(['signin']);
    })
  }


  getLocalStorage(){
    this.userData =   JSON.parse(localStorage.getItem('user')!);
  }

  crete_blog(data :any){
   
   this.getLocalStorage();
    let blog_id= '_'+Math.random().toString(36).substr(2, 9);

    data.uid = this.userData.uid
    data.timestamp = new Date;

    
    this.afs.collection('blogs').add({
         data
    }).then(res=>{
      console.log(res);
    })
    .catch(err=>{
      console.log(err);
    })
    // const userRef: AngularFirestoreDocument<any> = this.afs.doc(`blogs/${blog_id}`);

    // return userRef.set(data, {
    //   merge: false
    // })
  }


}
