import {NgModule} from '@angular/core';
import {NgbPaginationModule, NgbAlertModule, NgbToastModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    exports: [
        NgbPaginationModule,
        NgbAlertModule,
        NgbToastModule


    ],
})

export class NgbModule {}
