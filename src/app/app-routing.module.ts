import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'user',loadChildren:()=>import('./components/user-management/user-management-routing.module').then(m=>m.UserManagementRoutingModule)},
  {path: 'blog',loadChildren:()=>import('./components/blog/blog-routing.module').then(m=>m.BlogRoutingModule)},
  {path: '', redirectTo:'/blog',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
