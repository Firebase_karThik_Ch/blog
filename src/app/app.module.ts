import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';

import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { SETTINGS as AUTH_SETTINGS } from '@angular/fire/auth';
import { PERSISTENCE } from '@angular/fire/auth';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateBlogComponent } from './components/create-blog/create-blog.component';
import { ListBlogsComponent } from './components/list-blogs/list-blogs.component';
import { UserManagementModule } from './components/user-management/user-management.module';
import { NgbModule } from './nbg/ng-bootstrap';
import { MaterialModule } from './material/material';
import { BlogModule } from './components/blog/blog.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CreateBlogComponent,
    ListBlogsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    
    AngularFireModule.initializeApp(environment.firebaseConfig),
    
    BrowserAnimationsModule,
    MaterialModule,
    UserManagementModule,
    NgbModule,
    BlogModule,
    HttpClientModule,
    
    

  ],
  providers: [   
    { provide: PERSISTENCE, useValue: 'session' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
