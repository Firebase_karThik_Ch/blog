import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserManagementRoutingModule } from './user-management-routing.module';
import { SigninComponent } from './signin/signin.component';
import { MaterialModule } from 'src/app/material/material';
import {MatCardModule} from '@angular/material/card';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { NgbModule } from 'src/app/nbg/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SigninComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent
  ],
  imports: [
    CommonModule,
    UserManagementRoutingModule,
    MaterialModule,
    MatCardModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UserManagementModule { }
