import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { ToastServiceService } from 'src/app/services/toast-service.service';

declare const jQuery : any;
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  signinForm = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.email]),
    password: new FormControl('',[Validators.required])
  })
  signupForm = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.email]),
    password: new FormControl('',[Validators.required])
  })

  constructor(private api:ApiService,private  toast:ToastServiceService) { }
  showToast ="true"
  ngOnInit(): void {
    // this.toast.show('I am a success toast', { classname: 'bg-success text-light', delay: 1000 });
  
  }
  
  Onsignin() :any{
    console.log("ss");
    if(this.signinForm.invalid){
      this.toast.show('Please chek form once',{classname: 'bg-danger text-light'})
      return 0;
    }
    this.api.SignIn(this.signinForm.value);

    // this.toast.show('Login Successfull',{classname: 'bg-success text-light', delay: 1000})

  }
  Onsignup() :any{
    console.log(this.signupForm.value);
    if(this.signupForm.invalid){
      this.toast.show('Please chek form once',{classname: 'bg-danger text-light'})
      return 0;
    }
    this.api.SignUp(this.signupForm.value);

    // this.toast.show('Login Successfull',{classname: 'bg-success text-light', delay: 1000})

  }
  GoogleSignin(){
       this.api.GoogleAuth();

  }
  FacebookSignin(){
    this.api.FacebookAuth();

}
}
