import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateBlogmComponent } from './create-blogm/create-blogm.component';
import { ListblogsComponent } from './listblogs/listblogs.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';

const routes: Routes = [
  {path:'list',component:ListblogsComponent},
  {path:'create',component:CreateBlogmComponent},
  {path:'payment_success/:data',component:PaymentSuccessComponent},
  {path: '', component:ListblogsComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
