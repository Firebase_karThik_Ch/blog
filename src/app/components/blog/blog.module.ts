import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { ListblogsComponent } from './listblogs/listblogs.component';
import { CreateBlogmComponent } from './create-blogm/create-blogm.component';
import { MaterialModule } from 'src/app/material/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    ListblogsComponent,
    CreateBlogmComponent,
    PaymentSuccessComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class BlogModule { }
