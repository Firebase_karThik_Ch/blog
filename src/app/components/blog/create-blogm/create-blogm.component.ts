import { Component, OnInit } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

export interface Keywords {
  name: string;
}
@Component({
  selector: 'app-create-blogm',
  templateUrl: './create-blogm.component.html',
  styleUrls: ['./create-blogm.component.css']
})
export class CreateBlogmComponent implements OnInit {

  constructor(private api :ApiService) { }
  placeholderVar ="asd"
  CreateForm = new FormGroup({
    title : new FormControl('',Validators.required),
    short_description : new FormControl('',Validators.required),
    description : new FormControl('',Validators.required),
    keywords : new FormControl('',Validators.required)

  })
  ngOnInit(): void {
  }
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  keywords: Keywords[] = [
    
  ];
  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.keywords.push({name: value});
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(fruit: Keywords): void {
    const index = this.keywords.indexOf(fruit);

    if (index >= 0) {
      this.keywords.splice(index, 1);
    }
  }


  createPost(){

    this.api.crete_blog(this.CreateForm.value)

  }
}
