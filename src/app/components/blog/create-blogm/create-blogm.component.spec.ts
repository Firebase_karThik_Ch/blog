import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBlogmComponent } from './create-blogm.component';

describe('CreateBlogmComponent', () => {
  let component: CreateBlogmComponent;
  let fixture: ComponentFixture<CreateBlogmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateBlogmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBlogmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
