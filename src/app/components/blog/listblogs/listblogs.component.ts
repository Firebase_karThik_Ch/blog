import { HttpClient } from '@angular/common/http';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// declare const Buffer

@Component({
  selector: 'app-listblogs',
  templateUrl: './listblogs.component.html',
  styleUrls: ['./listblogs.component.css']
})
export class ListblogsComponent implements OnInit {

  paymentForm = new FormGroup({
    appId : new FormControl('895348d1ce82ff567ffa2673f43598',Validators.required),
    orderId: new FormControl(this.generate_orderId(),Validators.required),
    orderAmount: new FormControl('10',Validators.required),
    orderCurrency: new FormControl('INR',Validators.required),
    customerName: new FormControl('Karthik',Validators.required),
    customerPhone: new FormControl('703636143',Validators.required),
    customerEmail: new FormControl('chilakurikarthikeya@gmail.com',Validators.required),
    returnUrl: new FormControl('https://cashfreebackend.herokuapp.com/payment_result',Validators.required),
    notifyUrl: new FormControl('https://cashfreebackend.herokuapp.com/payment_result',Validators.required),
    signature: new FormControl(''),
  })

  constructor(private api : HttpClient,private router: Router) {
    
   }

  ngOnInit(): void {

 
  }

  generate_orderId(){
    console.log(Math.random())
    return Math.random();

  }

  get_signature(form: any, e: any){
    
    console.log(this.paymentForm.value)
    
    this.api.post("https://cashfreebackend.herokuapp.com/get_signature",this.paymentForm.value).subscribe(
      (res:any)=>{
          console.log(res)
          this.paymentForm.get('signature')?.setValue(res['signature']);
          console.log(this.paymentForm.value)
          // this.router.navigateByUrl('https://api.paypal.com')

          // window.location.href = 'https://test.cashfree.com/billpay/checkout/post/submit'
          // setTimeout(()=>{ 
            e.target.submit();                          // <<<---using ()=> syntax
        // }, 5000);        

          
         

      },err=>{
        console.log(err)

      }
    )

  }

}
