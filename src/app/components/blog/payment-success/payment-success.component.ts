import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.css']
})
export class PaymentSuccessComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
   data :any =""
  ngOnInit(): void {
    this.data = this.route.snapshot.paramMap.get('data')

  }

}
