import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastServiceService } from 'src/app/services/toast-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public toastService:ToastServiceService,public api:ApiService) { }
  isTemplate(toast:any) { return toast.textOrTpl instanceof TemplateRef; }

  ngOnInit(): void {
  }

}
